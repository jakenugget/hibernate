import java.util.*;

public class RunHibernateExample {

    public static void main(String[] args) {

        TestDAO t = TestDAO.getInstance();

        List<Player> c = t.getPlayers();
        for (Player i : c) {
            System.out.println(i);
        }

        System.out.println(t.getPlayer(1));
    }
}